Team Members:
Yiming Li (yl826)
Mason Ma (jm790)
Yufei Lei (yl721)
Chenying Li (cl590)

We choose the standard course project "Database-backed mini-amazon web app"

Team Name:
Go For Database

Link to gitlab:
https://gitlab.oit.duke.edu/yl826/mini-amazon

This project have a fully functional CI/CD pipeline, the production server address is http://vcm-25035.vm.duke.edu:5000/

How to Run the Code:
./install.sh
source env/bin/activate
flask run

For Flask part:
Basically we did not change any skeleton of this project.
So by running ./install.sh, everything should work.
The only thing we change is that this project using environment variable "DB_ADDRESS" in .flaskenv to specified which
db address to connect.

For db part:
 db/setup.sh should work.
 We have a test account(email: test@test.com, password:123456), and all other account's password is pass<user_id>,
 replace the "<user_id>" with the id of the user, no leading zero.