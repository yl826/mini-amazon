#!/bin/bash

mypath=`realpath $0`
mybase=`dirname $mypath`
cd $mybase

datadir="${1:-data/}"
if [ ! -d $datadir ] ; then
    echo "$datadir does not exist under $mybase"
    exit 1
fi

source ../.flaskenv
dbname=$DB_NAME
dbuser=$DB_USER
if [[ -n `psql -lqt | cut -d \| -f 1 | grep -w "$dbname"` ]]; then
    dropdb -U $dbuser $dbname
fi
createdb -U $dbuser $dbname

psql -U $dbuser -af create.sql $dbname
cd $datadir
psql -U $dbuser -af $mybase/load.sql $dbname
