import json
import datetime
import os.path

from werkzeug.security import generate_password_hash
import csv
from faker import Faker
import random

from db.generated.placeholder import PlaceholderPic

num_users = 500
num_purchases = 10000

Faker.seed(0)
fake = Faker()
balance_id = 0


def get_csv_writer(f):    return csv.writer(f, dialect='unix', quoting=csv.QUOTE_MINIMAL)


def gen_users(num_users,balance_id):
    seller = [0]
    buyer = []
    with open('../data/Users.csv', 'w') as f, open("../data/balance_transaction.csv", 'w') as b, open(
            "../data/seller.csv", 'w') as seller_csv:
        writer = get_csv_writer(f)
        balance_writer = get_csv_writer(b)
        seller_writer = get_csv_writer(seller_csv)

        # This the default test user
        writer.writerow(["0", "test@test.com", generate_password_hash("123456"), "test", "test", "704 15th St", 0])
        balance_writer.writerow([0, 0, datetime.datetime.now(), 100, ""])
        seller_writer.writerow([0])
        balance_id += 1

        print('Users...', end=' ', flush=True)
        for uid in range(1, num_users):
            if uid % 10 == 0:
                print(f'{uid}', end=' ', flush=True)
            profile = fake.profile()
            email = profile['mail']
            plain_password = f'pass{uid}'
            password = generate_password_hash(plain_password)
            name_components = profile['name'].split(' ')
            firstname = name_components[0]
            lastname = name_components[-1]
            address = profile['address']
            balance = random.randrange(10000)

            isSeller = bool(random.choice([True, False]))
            if isSeller:
                seller.append(uid)
                seller_writer.writerow([uid])
            else:
                buyer.append(uid)
            balance_writer.writerow([uid, uid, datetime.datetime.now(), balance, "topped_up"])
            writer.writerow([uid, email, password, firstname, lastname, address, 0])
        print(f'{num_users} generated')
    return seller, buyer


def gen_products_and_categories_and_inventory(seller_list):
    pid = 0
    cid = 0
    inventory_id = 0
    inventory_list = []
    product_list = []
    with open("products.json", "r") as read_file:
        data = json.load(read_file)
        with open('../data/Products.csv', 'w') as f, open('../data/Categories.csv', 'w') as c, open(
                '../data/Inventory.csv', 'w') as inventory_csv:
            writer = get_csv_writer(f)
            categories_writer = get_csv_writer(c)
            inventory_writer = get_csv_writer(inventory_csv)
            categories_writer.writerow([cid,'All'])
            cid += 1
            for key, values in data.items():
                categories_writer.writerow([cid, key])
                for name in values:
                    image_path = os.path.join('../../app/static/', str(pid) + '.png')
                    if not os.path.isfile(image_path):
                        placeholder = PlaceholderPic(name.split()[-1], font_size=150, size=600)
                        placeholder.image.save(image_path)
                    price = f'{str(fake.random_int(max=100))}.{fake.random_int(max=99):02}'
                    available = fake.random_element(elements=('true', 'false'))
                    description = fake.sentence(nb_words=5)[:-1]
                    writer.writerow([pid, "0", name, price, 'true', cid, description, str(pid) + '.png', '0'])
                    product_list.append({
                        "name": name,
                        "product_id": pid

                    })
                    inventory_seller_list = random.sample(seller_list, random.randrange(1, int(len(seller_list) / 4)))
                    for seller in inventory_seller_list:
                        qty = random.randrange(1, 100)
                        tmp_price = round(float(price) + random.uniform(-float(price) / 5, float(price) / 5), 2)
                        inventory_list.append({
                            "seller_id": seller,
                            "qty": qty,
                            "price": tmp_price,
                            "product_id": pid

                        })
                        inventory_writer.writerow([inventory_id, seller, pid, qty, tmp_price
                                                   ])
                        inventory_id += 1
                    pid += 1

                cid += 1
    return inventory_list, product_list


def gen_purchases(num_purchases, inventory_list,balance_id):
    order_list = []
    with open('../data/Purchases.csv', 'w') as f, open('../data/Orders.csv', 'w') as order_f, open(
            "../data/balance_transaction.csv", 'w') as balance_csv:
        writer = get_csv_writer(f)
        order_writer = get_csv_writer(order_f)
        balance_writer = get_csv_writer(balance_csv)
        print('Purchases...', end=' ', flush=True)
        oid = 0
        for id in range(num_purchases):
            if id % 100 == 0:
                print(f'{id}', end=' ', flush=True)
            uid = fake.random_int(min=0, max=num_users - 1)

            time_purchased = fake.date_time_between(start_date=datetime.datetime(2020, 12, 20, 14, 56, 26))
            writer.writerow([id, uid, time_purchased])
            total_price = 0
            for inventory in random.sample(inventory_list, random.randrange(1, 10)):
                purchase_id = id
                seller_id = inventory["seller_id"]
                product_id = inventory["product_id"]
                price_at_placement = inventory["price"]
                qty = random.randrange(1, 11)
                fulfillment_status = bool(random.choice([True, False]))

                order_list.append({
                    "seller_id": seller_id,
                    "buyer_id": uid
                })
                total_price += price_at_placement * qty
                order_writer.writerow(
                    [oid, purchase_id, seller_id, product_id, price_at_placement, qty, fulfillment_status,
                     datetime.datetime.now() if fulfillment_status else 'null'])

                oid += 1
            balance_writer.writerow([balance_id,uid,datetime.datetime.utcnow(),total_price,"Topped_up"])
            balance_id +=1
            balance_writer.writerow([balance_id,uid,datetime.datetime.utcnow(),-1*total_price,"Online Shopping"])
            balance_id += 1
        print(f'{num_purchases} generated')
    return order_list


def gen_product_review(seller_list, buyer_list, product_list):
    product_review_list = []
    with open('../data/reviewsforproducts.csv', 'w') as f:
        writer = get_csv_writer(f)
        print('reviewsforproducts...', end=' ', flush=True)
        id = 0
        reviewer_list = seller_list + buyer_list

        for product in product_list:

            product_id = product["product_id"]

            for user in random.sample(reviewer_list, random.randint(0, 5)):
                buyer_id = user
                rating = fake.random_int(min=1, max=5)
                upvote = 0
                description = fake.sentence(nb_words=5)[:-1]
                time_review = datetime.datetime.now()
                product_review_list.append({
                    "id": id
                })
                writer.writerow([id, buyer_id, product_id, rating, upvote, description, time_review])
                id += 1
    return product_review_list


def gen_seller_review(order_list):
    res_list = []
    seller_review_list = []
    with open('../data/reviewsforseller.csv', 'w') as f:
        writer = get_csv_writer(f)
        id = 0

        for order in order_list:
            buyer_id = order["buyer_id"]
            seller_id = order["seller_id"]
            tmp_dict = {
                "buyer_id": buyer_id,
                "seller_id": seller_id
            }
            if tmp_dict in res_list:
                continue
            res_list.append(tmp_dict)
            if random.choice([True, False, False]):
                continue
            rating = fake.random_int(min=1, max=5)
            upvote = 0
            description = fake.sentence(nb_words=5)[:-1]
            time_review = datetime.datetime.now()
            seller_review_list.append({
                "id": id,
                "buyer_id": buyer_id,
                "seller_id": seller_id
            })
            writer.writerow([id, buyer_id, seller_id, rating, upvote, description, time_review])
            id += 1
    return seller_review_list


def gen_product_review_upvote_transaction(product_review_list, buyer_list, seller_list):
    with open('../data/product_review_upvote_transaction.csv', 'w') as f:
        writer = get_csv_writer(f)
        id = 0
        all_user = buyer_list + seller_list
        for review in product_review_list:
            for user in random.sample(all_user, random.randint(0, 10)):
                amount = 1 * random.choice([1, 1, 1, 1, -1])
                writer.writerow([id, review["id"], user, datetime.datetime.now(), amount])
                id += 1


def gen_seller_review_upvote_transaction(seller_review_list, buyer_list, seller_list):
    with open('../data/seller_review_upvote_transaction.csv', 'w') as f:
        writer = get_csv_writer(f)
        id = 0
        all_user = buyer_list + seller_list
        for review in seller_review_list:
            for user in random.sample(all_user, random.randint(0, 10)):
                amount = 1 * random.choice([1, 1, 1, 1, -1])
                writer.writerow([id, review["id"], user, datetime.datetime.now(), amount])
                id += 1


seller_list, buyer_list = gen_users(num_users,balance_id)
inventory_list, product_list = gen_products_and_categories_and_inventory(seller_list)

order_list = gen_purchases(num_purchases, inventory_list,balance_id)
product_review_list = gen_product_review(seller_list, buyer_list, product_list)

gen_product_review_upvote_transaction(product_review_list, buyer_list, seller_list)

seller_review_list = gen_seller_review(order_list)

gen_seller_review_upvote_transaction(seller_review_list, buyer_list, seller_list)
