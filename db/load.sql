\COPY Users FROM 'Users.csv' WITH DELIMITER ',' NULL '' CSV
-- since id is auto-generated; we need the next command to adjust the counter
-- for auto-generation so next INSERT will not clash with ids loaded above:
SELECT pg_catalog.setval('public.users_id_seq',
                         (SELECT MAX(id)+1 FROM Users),
                         false);

\COPY Categories FROM 'Categories.csv' WITH DELIMITER ',' NULL '' CSV
SELECT pg_catalog.setval('public.categories_id_seq',
                         (SELECT MAX(id)+1 FROM Categories),
                         false);

\COPY Products FROM 'Products.csv' WITH DELIMITER ',' NULL '' CSV
SELECT pg_catalog.setval('public.products_id_seq',
                         (SELECT MAX(id)+1 FROM Products),
                         false);

\COPY Purchases FROM 'Purchases.csv' WITH DELIMITER ',' NULL '' CSV
SELECT pg_catalog.setval('public.purchases_id_seq',
                         (SELECT MAX(id)+1 FROM Purchases),
                         false);

\COPY Orders  FROM 'Orders.csv' WITH DELIMITER ',' NULL 'null' CSV
SELECT pg_catalog.setval('public.orders_id_seq',
                         (SELECT MAX(id)+1 FROM Orders),
                         false);

\COPY Inventory FROM 'Inventory.csv' WITH DELIMITER ',' NULL '' CSV
SELECT pg_catalog.setval('public.inventory_id_seq',
                         (SELECT MAX(id)+1 FROM Inventory),
                         false);

\COPY balance_transaction FROM 'balance_transaction.csv' WITH DELIMITER ',' NULL '' CSV
SELECT pg_catalog.setval('public.balance_transaction_id_seq',
                         (SELECT MAX(id)+1 FROM balance_transaction),
                         false);

\COPY Seller FROM 'Seller.csv' WITH DELIMITER ',' NULL '' CSV
SELECT pg_catalog.setval('public.seller_id_seq',
                         (SELECT MAX(id)+1 FROM Seller),
                         false);


\COPY reviewsforproducts FROM 'reviewsforproducts.csv' WITH DELIMITER ',' NULL '' CSV
SELECT pg_catalog.setval('public.reviewsforproducts_id_seq',
                         (SELECT MAX(id)+1 FROM reviewsforproducts),
                         false);


\COPY reviewsforseller FROM 'reviewsforseller.csv' WITH DELIMITER ',' NULL '' CSV
SELECT pg_catalog.setval('public.reviewsforseller_id_seq',
                         (SELECT MAX(id)+1 FROM reviewsforseller),
                         false);


\COPY product_review_upvote_transaction FROM 'product_review_upvote_transaction.csv' WITH DELIMITER ',' NULL '' CSV
SELECT pg_catalog.setval('public.product_review_upvote_transaction_id_seq',
                         (SELECT MAX(id)+1 FROM product_review_upvote_transaction),
                         false);

\COPY seller_review_upvote_transaction FROM 'seller_review_upvote_transaction.csv' WITH DELIMITER ',' NULL '' CSV
SELECT pg_catalog.setval('public.seller_review_upvote_transaction_id_seq',
                         (SELECT MAX(id)+1 FROM seller_review_upvote_transaction),
                         false);