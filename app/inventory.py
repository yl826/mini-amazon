from flask import render_template, Blueprint, redirect, url_for, flash, request
from flask_login import current_user
from .models.inventory import Inventory
from wtforms import DecimalField, IntegerField, SubmitField
from wtforms.validators import InputRequired, NumberRange
from flask_wtf import FlaskForm

bp = Blueprint('inventory', __name__)

@bp.route('/inventory/', methods=['GET','POST'])
def inventory():
    if request.method == 'POST':
        if request.form['action'] == 'remove':
            Inventory.remove_product(request.form['sid'], request.form['pid'])
    inventory = Inventory.get_product_sid(current_user.id)
    return render_template("inventory.html", inventory=inventory)


class AddExistedProductForm(FlaskForm):
    pid = IntegerField('Product ID', validators=[InputRequired()])
    price = DecimalField('Price', validators=[InputRequired(), NumberRange(min=0, message="Price should >= $0")])
    quantity = IntegerField('Quantity',
                            validators=[InputRequired(), NumberRange(min=0, message="Quantity should >= 0")])
    submit = SubmitField('Confirm')


@bp.route('/add-existed-Product', methods=['GET', 'POST'])
def addExistedProduct():
    newly_added_pid = -1
    if request.method == 'GET':
        newly_added_pid = request.args.get('pid', -1)
    sid = current_user.id
    form = AddExistedProductForm()
    if form.validate_on_submit():
        new_product = Inventory.add_existed_product(form.pid.data, sid, form.price.data, form.quantity.data)
        if new_product is False:
            flash('Error: Product already existed.')
            return redirect(url_for('inventory.addExistedProduct', sid=sid))
        flash('Successfully added new product.')
        return redirect(url_for("inventory.addExistedProduct", sid=sid))
    return render_template('addExistedProduct.html', title='Add Existed Product', form=form, sid=sid,
                           newly_added_pid=newly_added_pid)

class EditInventoryForm(FlaskForm):
    price = DecimalField('Enter New Price', validators=[InputRequired(), NumberRange(min=0, message="Price should >= $0")])
    quantity = IntegerField('Enter New Quantity', validators=[InputRequired(), NumberRange(min=0, message="Quantity should >= 0")])
    submit = SubmitField('Confirm')

@bp.route('/edit-inventory/<pid>/<sid>', methods=['GET', 'POST'])
def editInventory(pid, sid):
    inventory = Inventory.get_product_pid(pid)
    form = EditInventoryForm()
    if form.validate_on_submit():
        Inventory.change_price(pid, sid, form.price.data)
        Inventory.change_quantity(pid, sid, form.quantity.data)
        flash("Successfully changed")
        return redirect(url_for('inventory.inventory', sid=sid))
    return render_template('editInventory.html', form=form, inventory=inventory, pid=pid, sid=sid)



