from flask import Flask
from flask_login import LoginManager
from .config import Config
from .db import DB

UPLOAD_FOLDER = 'static/'
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif', 'heif'}

login = LoginManager()
login.login_view = 'users.login'


def create_app():
    app = Flask(__name__)
    app.config.from_object(Config)
    app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

    app.db = DB(app)
    login.init_app(app)

    from .index import bp as index_bp
    app.register_blueprint(index_bp)

    from .users import bp as user_bp
    app.register_blueprint(user_bp)

    from .product import bp as product_bp
    app.register_blueprint(product_bp)

    from .inventory import bp as inventory_bp
    app.register_blueprint(inventory_bp)

    from .result import bp as result_bp
    app.register_blueprint(result_bp)

    from .cart import bp as cart_bp
    app.register_blueprint(cart_bp)

    from .product_editor import bp as product_editor_bp
    app.register_blueprint(product_editor_bp)

    from .reviews import bp as reviews_bp
    app.register_blueprint(reviews_bp)

    from .orders import bp as orders_bp
    app.register_blueprint(orders_bp)

    from .seller import bp as seller_bp
    app.register_blueprint(seller_bp)

    from .purchase import bp as purchase_bp
    app.register_blueprint(purchase_bp)

    from .inventory_analyze import bp as inventory_analyze_bp
    app.register_blueprint(inventory_analyze_bp)

    from .api import bp as api_bp
    app.register_blueprint(api_bp)

    return app
