from flask import render_template, redirect, url_for, flash
from wtforms import SubmitField, SelectField
from flask_wtf import FlaskForm

from .models.seller import Seller


from flask import Blueprint
bp = Blueprint('seller', __name__)

@bp.route('/seller/<sid>', methods=['GET'])
def seller(sid):
    seller_verify = Seller.seller_verify(sid)
    if seller_verify == False:
        return render_template("seller_register_page.html", sid=sid)
    else:
        return render_template("seller.html", sid=sid)

class becomeSellerForm(FlaskForm):
    confirm = SelectField('Do you want to become a seller?', choices=["Yes", "No"])
    submit = SubmitField('Confirm')

@bp.route('/become-seller/<uid>', methods=['GET', 'POST'])
def becomeSeller(uid):
    form = becomeSellerForm()
    confirmation = form.confirm.data
    if form.validate_on_submit():
        if confirmation == "No":
            return redirect(url_for("seller.becomeSeller", uid=uid))
        else:
            beSeller = Seller.become_seller(uid)
            if beSeller == False:
                flash("You are already a seller")
                return redirect(url_for("seller.becomeSeller", uid=uid))
            flash("You are now a seller, Please re-click Seller Page to refresh.")
            return redirect(url_for("seller.becomeSeller", uid=uid))
    return render_template("become_seller.html", form=form, uid=uid)