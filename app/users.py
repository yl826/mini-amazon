import datetime

from flask import render_template, redirect, url_for, flash, request, abort
from werkzeug.urls import url_parse
from flask_login import login_user, logout_user, current_user
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, IntegerField, DateTimeField, validators
from wtforms.fields.html5 import DateField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo

from .models.balance_transaction import BalanceTransaction
from .models.order import Order
from .models.purchase import Purchase
from .models.review import SellerReview
from .models.seller import Seller
from .models.user import User

from flask import Blueprint

bp = Blueprint('users', __name__)


class LoginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')


@bp.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index.index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.get_by_auth(form.email.data, form.password.data)
        if user is None:
            flash('Invalid email or password')
            return redirect(url_for('users.login'))
        login_user(user)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index.index')

        return redirect(next_page)
    return render_template('login.html', title='Sign In', form=form)


class RegistrationForm(FlaskForm):
    firstname = StringField('First Name', validators=[DataRequired()])
    lastname = StringField('Last Name', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(),
                                       EqualTo('password')])
    address = StringField('address', validators=[DataRequired()])
    submit = SubmitField('Register')

    def validate_email(self, email):
        if User.email_exists(email.data):
            raise ValidationError('Already a user with this email.')


@bp.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index.index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        if User.register(form.email.data,
                         form.password.data,
                         form.firstname.data,
                         form.lastname.data, form.address.data):
            flash('Congratulations, you are now a registered user!')
            return redirect(url_for('users.login'))
    return render_template('register.html', title='Register', form=form)


class EditProfileForm(FlaskForm):
    current_user_id = None
    firstname = StringField('First Name', validators=[DataRequired()])
    lastname = StringField('Last Name', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    ori_password = PasswordField('Current Password', validators=[DataRequired()])
    password = PasswordField('New Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat New Password', validators=[DataRequired(),
                                           EqualTo('password')])
    address = StringField('address', validators=[DataRequired()])
    submit = SubmitField('Update Profile')

    def validate_email(self, email):
        if User.email_exists_except_current_user(email.data, self.current_user_id):
            raise ValidationError('Already a user with this email.')

    def validate_ori_password(self, ori_password):
        if not User.is_ori_password_valid(ori_password.data, self.current_user_id):
            raise ValidationError('Current Password invalid!')


@bp.route('/editProfile', methods=['GET', 'POST'])
def editProfile():
    if not current_user.is_authenticated:
        return redirect(url_for('index.index'))

    # Load current user information from db with user_id
    user = User.get(current_user.id)

    # Restore the current information
    form = EditProfileForm(firstname=user.firstname, lastname=user.lastname, email=user.email, address=user.address)

    # set the
    form.current_user_id = current_user.id

    if form.validate_on_submit():
        if User.updateProfile(form.email.data,
                              form.password.data,
                              form.firstname.data,
                              form.lastname.data,
                              form.address.data,
                              current_user.id):
            flash('Profile update successfully! Please use your new password to login.')
            logout_user()
            return redirect(url_for('users.login'))
    return render_template('editProfile.html', title='Register', form=form)


class WithdrawForm(FlaskForm):
    current_user_id = None
    current_balance = None
    withdraw_amount = IntegerField('Withdraw Amount', validators=[])
    submit_withdraw = SubmitField('Withdraw')

    def validate_withdraw_amount(self, amount):
        if amount.data <= 0:
            raise ValidationError('Withdraw amount must greater than 0')
        if amount.data > self.current_balance:
            raise ValidationError('Withdraw amount must less than current balance')


class ToppUpForm(FlaskForm):
    current_user_id = None
    current_balance = None
    topped_up_amount = IntegerField('Topped up Amount', validators=[])
    submit_topped_up = SubmitField('Topped up')

    def validate_topped_up_amount(self, amount):
        if amount.data <= 0:
            raise ValidationError('Topped up amount must greater than 0')


@bp.route('/balance', methods=['GET', 'POST'])
def balance():
    if not current_user.is_authenticated:
        return redirect(url_for('index.index'))

    # Load current user information from db with user_id
    user = User.get(current_user.id)
    withdraw_form = WithdrawForm()
    withdraw_form.current_user_id = current_user.id
    withdraw_form.current_balance = user.balance

    topped_up_form = ToppUpForm()
    topped_up_form.current_user_id = current_user.id
    topped_up_form.current_balance = user.balance

    if withdraw_form.submit_withdraw.data and withdraw_form.validate():
        try:
            BalanceTransaction.add_transaction(current_user.id, withdraw_form.withdraw_amount.data * -1, "withdraw")
        except Exception as error:
            flash('Current balance is less than withdraw amount!')
            return redirect(url_for('users.balance'))
        flash('Withdraw successfully!')
        return redirect(url_for('users.balance'))

    if topped_up_form.submit_topped_up.data and topped_up_form.validate():
        BalanceTransaction.add_transaction(current_user.id, topped_up_form.topped_up_amount.data, "topped_up")
        flash('Topped up successfully!')
        return redirect(url_for('users.balance'))

    balance_transaction = BalanceTransaction.get_for_user(current_user.id)
    spending_amount = 0
    for transaction in balance_transaction:
        if transaction.type_of_transaction == "Online Shopping":
            spending_amount += transaction.amount * -1
    return render_template('balance.html', balance=user.balance, withdraw_form=withdraw_form,
                           topped_up_form=topped_up_form, balance_transaction=balance_transaction,
                           spending_amount=spending_amount)


@bp.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index.index'))


@bp.route('/user/<user_id>', methods=['GET', 'POST'])
def user_public_view(user_id):
    try:
        user = User.get(user_id)
    except Exception as e:
        abort(404, description="Resource not found")
    if user is None:
        abort(404, description="Resource not found")

    if request.method == 'POST' and current_user.is_authenticated:
        SellerReview.update_upvote_for_id(int(request.form['review_id']),
                                          1 if request.form['action'] == 'upvote' else -1, current_user.id)

    isSeller = Seller.seller_verify(user_id)

    seller_reviews = SellerReview.get_for_seller(user_id)
    avg_rating = 0 if len(seller_reviews) == 0 else sum(review.rating for review in seller_reviews) / len(
        seller_reviews)

    previous_review = SellerReview.get_for_product_and_buyer(user_id, current_user.id)
    isOrderedFromSeller = Order.isUserOrderedFromSeller(current_user.id, user_id)
    return render_template('user_public_view.html', user=user, isSeller=isSeller, reviews=seller_reviews,
                           avg_rating=avg_rating,
                           isNewReview=previous_review is None, isOrderedFromSeller=isOrderedFromSeller)


class PurchaseHistorySearchForm(FlaskForm):
    item = StringField('Item Name', validators=[validators.Optional()])
    seller = StringField('Seller Name', validators=[validators.Optional()])
    start_date = DateField('Start Date', format='%Y-%m-%d', validators=[validators.Optional()])
    end_date = DateField('End Date', format='%Y-%m-%d', validators=[validators.Optional()])

    submit = SubmitField('Search')


@bp.route('/purchaseHistory', methods=['GET', 'POST'])
def purchase_history():
    if not current_user.is_authenticated:
        return redirect(url_for('index.index'))

    form = PurchaseHistorySearchForm()

    seller_name = "" if form.seller.data is None else form.seller.data
    item_name = "" if form.item.data is None else form.item.data
    start_date = datetime.datetime(1980, 9, 14, 0, 0, 0) if form.start_date.data is None else form.start_date.data
    end_date = (datetime.datetime.today() if form.end_date.data is None else form.end_date.data) + datetime.timedelta(
        days=1)
    purchases = Purchase.get_all_by_uid_since(
        current_user.id, item_name, seller_name, start_date, end_date)

    return render_template('purchase_history.html', purchase_history=purchases, form=form)
