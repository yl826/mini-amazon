from flask import render_template, request, redirect, url_for
from flask_login import current_user
from .models.cart import Cart
from .models.category import Categories
from sys import stderr

from flask import Blueprint

bp = Blueprint('cart', __name__)


@bp.route('/cart', methods=['POST', 'GET'])
def cart():
    is_checked_out_just_now = False
    checkout_error = False
    if request.method == 'POST' and current_user.is_authenticated:
        if request.form['action'] == 'add':
            # Add an item to cart
            inventory_id = request.form['inventory_id']
            product_id = request.form['product_id']
            qty = request.form['qty']
            Cart.add_to_cart(current_user.id, inventory_id, qty, product_id)
        if request.form['action'] == 'remove':
            inventory_id = request.form['inventory_id']
            Cart.remove_from_cart(current_user.id, inventory_id)
        if request.form['action'] == 'toggle_save_for_later':
            inventory_id = request.form['inventory_id']
            Cart.toggle_save_for_later(current_user.id, inventory_id)
        if request.form['action'] == 'checkout':
            try:
                purchase_id = Cart.checkout(current_user.id)
                #is_checked_out_just_now = True
                return redirect(f"{url_for('purchase_details.purchase_details',purchase_id=purchase_id)}", code=302)
            except Exception as e:
                checkout_error = True
                print(e, file=stderr)
        if request.form['action'] == 'change_qty':
            new_qty = request.form['qty']
            if int(new_qty) >= 1:
                Cart.change_qty(current_user.id, request.form['inventory_id'], new_qty)
    _cart = []
    _saved_for_later = []
    if current_user.is_authenticated:
        # Get cart for the current logged-in user
        _cart = Cart.get_cart_for_user(current_user.id)
        _saved_for_later = Cart.get_cart_for_user(current_user.id, True)
    else:
        pass
    categories = Categories.get_all()
    total_price = sum([i.product_price * i.qty for i in _cart]) if len(_cart) > 0 else 0
    # render the page by adding information to the index.html file
    return render_template('cart.html',
                           cart=_cart,
                           saved_for_later=_saved_for_later,
                           categories=categories,
                           is_checked_out_just_now=is_checked_out_just_now,
                           total_price=total_price,
                           checkout_error=checkout_error)
