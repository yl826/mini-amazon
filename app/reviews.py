from flask import render_template, request, abort
from flask_login import current_user
from flask import Blueprint, redirect, url_for

from .models.review import SellerReview, ProductReview

bp = Blueprint('reviews', __name__)


@bp.route('/review', methods=['GET', 'POST'])
def review():
    if not current_user.is_authenticated:
        return redirect(url_for('index.index'))

    if request.method == 'POST':
        if request.form['action'] == 'delete_seller_review':
            SellerReview.delete_by_id(request.form['review_id'])
        else:
            ProductReview.delete_by_id(request.form['review_id'])

    seller_review = SellerReview.get_for_user(current_user.id)
    product_review = ProductReview.get_for_user(current_user.id)
    return render_template('reviews.html', seller_review=seller_review, product_review=product_review)


@bp.route('/addReview', methods=['GET', 'POST'])
def addReview():
    if not current_user.is_authenticated:
        return redirect(url_for('index.index'))

    review_type = request.args.get('type')
    product_or_seller_id = request.args.get('id')
    if review_type is None or product_or_seller_id is None or review_type != "product" and review_type != "seller":
        abort(404, description="Resource not found")

    previous_review = None
    if review_type == "product":
        previous_review = ProductReview.get_for_product_and_buyer(product_or_seller_id, current_user.id)
    else:
        previous_review = SellerReview.get_for_product_and_buyer(product_or_seller_id, current_user.id)

    # Handle the post request
    if request.method == 'POST':
        if review_type == "product":
            ProductReview.add(current_user.id,
                              product_or_seller_id,
                              request.form['rating'],
                              request.form['comment'])
        else:
            SellerReview.add(current_user.id,
                             product_or_seller_id,
                             request.form['rating'],
                             request.form['comment'])
        return redirect(url_for('reviews.review'))
    return render_template('addOrUpdateReview.html', isNewReview=previous_review is None, previous_review=previous_review)
