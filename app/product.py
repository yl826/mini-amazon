import math

from flask import render_template, request
from flask_login import current_user
from .models.product import Product
from .models.category import Categories
from .models.inventory import Inventory
from .models.review import ProductReview
from .models.order import Order

from flask import Blueprint

bp = Blueprint('product', __name__)


@bp.route('/product_browser/')
def browser():
    sort_options = [
        {
            "name": "Featured",
            "filed": "id",
            "order": "ASC"
        },
        {
            "name": "Price: Low to High",
            "filed": "price",
            "order": "ASC"
        },
        {
            "name": "Price: High to Low",
            "filed": "price",
            "order": "DESC"
        },
        {
            "name": "Avg. Customer Review",
            "filed": "avg_rating",
            "order": "DESC"
        },

    ]

    limit = int(request.args.get('limit') if request.args.get('limit') is not None else 40)
    page = int(request.args.get('page') if request.args.get('page') is not None else 1) - 1
    search_keyword = request.args.get('search') if request.args.get('search') is not None else ''
    category = request.args.get('category') if request.args.get('category') is not None and request.args.get(
        'category') != '' and request.args.get(
            'category') != 'None' else 'All'
    sort = request.args.get('sort') if request.args.get('sort') is not None else 'Featured'
    start_price = abs(
        float(request.args.get('start_price')) if request.args.get('start_price') is not None and request.args.get(
            'start_price') != '' and request.args.get(
            'start_price') != 'None' else 0)
    end_price = abs(
        float(request.args.get('end_price')) if request.args.get('end_price') is not None and request.args.get(
            'end_price') != '' and request.args.get(
            'end_price') != 'None' else 9999999999.99)
    review_above = request.args.get('review_above') if request.args.get(
        'review_above') is not None and request.args.get(
        'review_above') != '' and request.args.get(
            'review_above') != 'None' else 0

    if category == 'All':
        category = '%'

    num_products = Product.get_total_number(True, keyword=search_keyword, category=category, start_price=start_price,
                                            end_price=end_price, review_above=review_above)
    max_page = math.ceil(num_products / limit)

    # get all available products for sale:
    products = Product.get_all(True, limit, page * limit, search_keyword, category,
                               sort_field=next(item for item in sort_options if item["name"] == sort)["filed"],
                               sort_order=next(item for item in sort_options if item["name"] == sort)["order"],
                               start_price=start_price, end_price=end_price, review_above=review_above)
    product_id_list = []
    for product in products:
        product_id_list.append(product.id)

    nums_review_over_product = []
    if len(products) > 0:
        nums_review_over_product = ProductReview.get_total_number_by_id(product_id_list)
        for product in nums_review_over_product:
            for x in products:
                if x.id == product[0]:
                    x.total_reviews = product[1]

    print(nums_review_over_product)
    # get all categories
    categories = Categories.get_all()
    # render the page by adding information to the index.html file

    return render_template('product_browser.html', avail_products=products, categories=categories,
                           num_products=num_products, max_page=max_page, sort_options=sort_options)


@bp.route('/product')
@bp.route('/product/<pid>', methods=['POST', 'GET'])
def product(pid=None):
    if request.method == 'POST' and current_user.is_authenticated:
        if request.form['action'] == 'add_review':
            ProductReview.add(current_user.id,
                              pid,
                              request.form['rating'],
                              request.form['comment'])
        elif request.form['action'] == 'update_review':
            ProductReview.update_for_product_and_buyer(pid,
                                                       current_user.id,
                                                       request.form['rating'],
                                                       request.form['comment'])
        elif request.form['action'] == 'down_vote':
            if int(request.form['likes']) > 0:
                ProductReview.update_upvote_for_id(int(request.form['review_id']), -1, current_user.id)
        elif request.form['action'] == 'upvote':
            ProductReview.update_upvote_for_id(int(request.form['review_id']), 1, current_user.id)
        elif request.form['action'] == 'delete_review':
            ProductReview.delete_for_id(int(request.form['review_id']))
    _product = Product.get(pid)
    # get all categories
    categories = Categories.get_all()
    category_name = Categories.get_by_id(_product.cid).name
    # get all available sellers for the current product
    inventory = Inventory.get_all_sellers_for_product(int(pid))
    # get all product reviews
    reviews = ProductReview.get_for_product(int(pid))

    default_rating = 5
    default_comments = ""
    default_review_submit_action = "add_review"
    my_review = None
    is_adding_new_review_allowed = False
    if current_user.is_authenticated:
        my_review = ProductReview.get_for_product_and_buyer(int(pid), int(current_user.id))
        if my_review is not None:
            default_rating = my_review.rating
            default_comments = my_review.description
            default_review_submit_action = "update_review"

        prev_orders = Order.get_by_product_id_and_buyer_id(pid, current_user.id)
        is_adding_new_review_allowed = (len(prev_orders) > 0)

    # previous_review = ProductReview.get_for_product_and_buyer(pid, current_user.id) why add this crap?
    # render the page by adding information to the index.html file
    return render_template('productDetail.html',
                           isNewReview=my_review is None,
                           product=_product,
                           categories=categories,
                           category_name=category_name,
                           inventory=inventory,
                           reviews=reviews,
                           default_comments=default_comments,
                           default_rating=default_rating,
                           default_review_submit_action=default_review_submit_action,
                           is_adding_new_review_allowed=is_adding_new_review_allowed)
