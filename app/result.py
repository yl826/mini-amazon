from flask import render_template, request
from .models.product import Product
from .models.category import Categories

from flask import Blueprint

bp = Blueprint('result', __name__)


@bp.route('/result', methods=['GET'])
def search():
    products = []
    get_param = '?'
    page_title = 'Filter for null'
    if request.args.get('action') == 'search':
        query = request.args.get('search')
        sort_by = request.args.get('sort_by', None)
        products = Product.search(query, sort_by)
        page_title = f'Search Result for {query}'
        get_param += f'action=search&search={query}'
    elif request.args.get('category_id', "") != "":
        category_id = request.args.get('category_id')
        sort_by = request.args.get('sort_by', None)
        products = Product.search_cid(int(category_id), sort_by)
        selected_category = Categories.get_by_id(int(category_id))
        page_title = f'Products for Category: {selected_category.name}'
        get_param += f'category_id={category_id}'
    # get all categories
    categories = Categories.get_all()

    # render the page by adding information to the index.html file
    return render_template('result.html',
                           avail_products=products,
                           categories=categories,
                           page_title=page_title,
                           get_param=get_param)
