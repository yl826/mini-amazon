from flask import render_template, request
from .models.order import Order
from flask import Blueprint

bp = Blueprint('orders', __name__)

@bp.route('/orders/<sid>/<purchase_id>', methods=['GET','POST'])
def orders(sid, purchase_id):
    if request.method == 'POST':
        if request.form['action'] == 'fulfill':
            Order.item_fulfilled(request.form['id'])
    orders = Order.get_by_seller_id(sid, purchase_id)
    return render_template("orders.html", orders = orders, purchase_id = purchase_id)



