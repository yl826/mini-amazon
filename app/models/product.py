from flask import current_app as app


class Product:
    def __init__(self, id, name, price, available, cid, description, image_path, creator_id, avg_rating):
        self.id = id
        self.name = name
        self.price = price
        self.available = available
        self.cid = cid
        self.description = description
        self.image_path = image_path
        self.creator_id = creator_id
        self.avg_rating = avg_rating

    @staticmethod
    def get(id):
        rows = app.db.execute('''
            SELECT id, name, price, available, cid, description, image_path, creator_id, avg_rating
            FROM Products
            WHERE id = :id
        ''', id=id)
        return Product(*(rows[0])) if rows is not None else None

    @staticmethod
    def get_total_number(available=True, limit: int = 40, offset: int = 0, keyword='', category='%', start_price=0,
                         end_price=9999999999.99,review_above=0):
        rows = app.db.execute('''
            SELECT count(*)
            FROM Products join categories on Products.cid = categories.id
            WHERE available = :available 
            AND (Products.name LIKE '%' || :keyword || '%' OR Products.description LIKE '%' || :keyword || '%')
            AND categories.name like :category
            AND price between :start_price and :end_price
            AND avg_rating >= :review_above

        ''', available=available, limit=limit, offset=offset, keyword=keyword, category=category,
                              start_price=start_price, end_price=end_price,review_above=review_above)
        return rows[0][0] if rows[0][0] is not None else 0

    @staticmethod
    def get_all(available=True, limit: int = 40, offset: int = 0, keyword='', category='%', sort_field='id',
                sort_order='ASC', start_price=0, end_price=9999999999.99,review_above=0):
        rows = app.db.execute('''
            SELECT Products.id, Products.name, price, available, cid, description, image_path, creator_id, avg_rating
            FROM Products join categories on Products.cid = categories.id
            WHERE available = :available 
            AND (Products.name LIKE '%' || :keyword || '%' OR Products.description LIKE '%' || :keyword || '%')
            AND categories.name like :category
            AND price between :start_price and :end_price
            AND avg_rating >= :review_above
            order by 

                case when :sort_field = 'price' and :sort_order = 'DESC' THEN  price end DESC,
                case when :sort_field = 'price' and :sort_order = 'ASC' THEN  price end ASC,
                case when :sort_field = 'id' and :sort_order = 'ASC' THEN  Products.id end ASC,
                case when :sort_field = 'avg_rating' and :sort_order = 'DESC' THEN  avg_rating end DESC
            LIMIT :limit
            OFFSET :offset
            
        ''', available=available, limit=limit, offset=offset, keyword=keyword, category=category,
                              sort_field=sort_field, sort_order=sort_order, start_price=start_price,
                              end_price=end_price,review_above=review_above)
        return [Product(*row) for row in rows]

    @staticmethod
    def search(query: str, sort_by=None):
        if type(sort_by) is str and sort_by.find(';') != -1:
            sort_by = None
        rows = app.db.execute(f'''
            SELECT id, name, price, available, cid, description, image_path, creator_id, avg_rating
            FROM Products
            WHERE Products.name LIKE :query OR Products.description LIKE :query
            {("ORDER BY " + sort_by) if sort_by is not None else ""};
        ''', query='%' + query + '%')
        return [Product(*row) for row in rows]

    @staticmethod
    def search_cid(cid: int, sort_by=None):
        if type(sort_by) is str and sort_by.find(';') != -1:
            sort_by = None
        rows = app.db.execute(f'''
            SELECT id, name, price, available, cid, description, image_path, creator_id, avg_rating
            FROM Products
            WHERE cid = :cid
            {("ORDER BY " + sort_by) if sort_by is not None else ""};
        ''', cid=cid)
        return [Product(*row) for row in rows]

    @staticmethod
    def add(name: str, creator_id: int, cid: int, description: str, image_path: str):
        if image_path is None:
            rows = app.db.execute('''
                INSERT INTO Products(creator_id, name, cid, description)
                VALUES(:creator_id, :name, :cid, :description)
                RETURNING id; ''', creator_id=creator_id,
                                  name=name,
                                  cid=cid,
                                  description=description)
        else:
            rows = app.db.execute('''
                INSERT INTO Products(creator_id, name, cid, description, image_path)
                VALUES(:creator_id, :name, :cid, :description, :image_path)
                RETURNING id; ''', creator_id=creator_id,
                                  name=name,
                                  cid=cid,
                                  description=description,
                                  image_path=image_path)
        id = rows[0][0]
        return id if rows else None

    @staticmethod
    def update(pid: int, name: str, creator_id: int, cid: int, description: str, image_path: str):
        if image_path is None:
            rows = app.db.execute('''
                UPDATE Products SET (creator_id, name, cid, description) = 
                (:creator_id, :name, :cid, :description)
                WHERE id = :pid
                RETURNING id; ''', creator_id=creator_id,
                                  name=name,
                                  cid=cid,
                                  description=description,
                                  pid=pid)
        else:
            rows = app.db.execute('''
                UPDATE Products SET (creator_id, name, cid, description, image_path) = 
                (:creator_id, :name, :cid, :description, :image_path)
                WHERE id = :pid
                RETURNING id; ''', creator_id=creator_id,
                                  name=name,
                                  cid=cid,
                                  description=description,
                                  image_path=image_path,
                                  pid=pid)
            # id = rows[0][0]
            return None
