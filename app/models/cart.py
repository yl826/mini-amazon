from flask import current_app as app
from .balance_transaction import BalanceTransaction


class Cart:
    def __init__(self, product_id, product_name, buyer_id, inventory_id, seller_firstname, seller_lastname, qty, is_saved_for_later, product_price=0):
        self.product_id = product_id
        self.product_name = product_name
        self.buyer_id = buyer_id
        self.inventory_id = inventory_id
        self.seller_name = seller_firstname + " " + seller_lastname
        self.qty = qty
        self.is_saved_for_later = is_saved_for_later,
        self.product_price = product_price

    @staticmethod
    def get_cart_for_user(buyer_id, saved_for_later=False):
        rows = app.db.execute('''
        SELECT Carts.product_id AS product_id, Products.name AS product_name, Carts.buyer_id AS buyer_id, Carts.inventory_id AS inventory_id, Users.firstname, Users.lastname, Carts.qty, Carts.is_saved_for_later, Inventory.price
        FROM Carts, Products, Inventory, Users
        WHERE Carts.product_id = Products.id
        AND Carts.inventory_id = Inventory.id
        AND Inventory.sid = Users.id
        AND Carts.buyer_id = :buyer_id
        AND Carts.is_saved_for_later = :saved_for_later
        ''', buyer_id=buyer_id, saved_for_later=saved_for_later)
        return [Cart(*row) for row in rows]

    @staticmethod
    def add_to_cart(buy_id, inventory_id, qty, product_id):
        rows = app.db.execute('''SELECT qty FROM Carts WHERE inventory_id=:inventory_id AND buyer_id=:buy_id;''', inventory_id=inventory_id, buy_id=buy_id)
        if rows is None or len(rows) == 0:
            query = f'''INSERT INTO Carts(id, product_id, buyer_id, inventory_id, qty, is_saved_for_later)
                    VALUES (COALESCE((SELECT MAX(id)+1 FROM Carts),0), {product_id}, {buy_id}, {inventory_id}, {qty}, FALSE);'''
        else:
            query = f'''UPDATE Carts
                        SET qty = {int(rows[0][0]) + int(qty)}
                        WHERE inventory_id={inventory_id} AND buyer_id={buy_id};'''
        rows = app.db.execute(query)
        return rows

    @staticmethod
    def remove_from_cart(buy_id, inventory_id):
        rows = app.db.execute('''DELETE FROM Carts WHERE inventory_id=:inventory_id AND buyer_id=:buy_id;''',
                              inventory_id=inventory_id,
                              buy_id=buy_id)
        return rows

    @staticmethod
    def toggle_save_for_later(buy_id, inventory_id):
        rows = app.db.execute(
            ''' UPDATE Carts 
                SET is_saved_for_later= (NOT is_saved_for_later)
                WHERE inventory_id=:inventory_id AND buyer_id=:buy_id;''',
            inventory_id=inventory_id,
            buy_id=buy_id)
        return rows

    @staticmethod
    def change_qty(buy_id, inventory_id, qty):
        rows = app.db.execute('''UPDATE Carts 
                                 SET qty = :qty 
                                 WHERE inventory_id=:inventory_id AND buyer_id=:buy_id;''',
                              buy_id=buy_id,
                              inventory_id=inventory_id,
                              qty=qty)
        return rows

    @staticmethod
    def checkout(buy_id):
        rows = app.db.execute('''SELECT Carts.id, Products.id, Carts.inventory_id, Carts.qty, is_saved_for_later, Inventory.price, inventory.sid, inventory.id
                                    FROM Carts, Products, Inventory
                                    WHERE Carts.product_id = Products.id
                                    AND Carts.inventory_id = Inventory.id
                                    AND Carts.is_saved_for_later = FALSE
                                    AND Carts.buyer_id = :buy_id;''',
                              buy_id=buy_id)
        if rows is None or len(rows) == 0:
            return None
        purchase_id = app.db.execute(f'''INSERT INTO Purchases (buyer_id) VALUES ({buy_id}) RETURNING id;''')[0][0]
        values = [f'({purchase_id}, {i[6]}, {i[1]}, {i[5]}, {i[3]})' for i in rows]
        query = f'''INSERT INTO Orders(purchase_id, seller_id, product_id, price_at_placement, qty)
                VALUES {','.join(values)};'''
        app.db.execute(query)

        queries = [f'''UPDATE Inventory SET quantity = quantity - {i[3]} WHERE id = {i[-1]};''' for i in rows]
        app.db.execute(' '.join(queries))
        # Update balance for the buyer and sellers
        financial_flow = [(i[3] * i[5], i[6]) for i in rows]
        # Update for the buyer
        BalanceTransaction.add_transaction(buy_id,
                                           -1 * sum(total for total, _ in financial_flow),
                                           "Online Shopping")
        # Update for sellers
        for total, seller_id in financial_flow:
            BalanceTransaction.add_transaction(seller_id, total, "Income")

        rows = app.db.execute(f'''DELETE FROM Carts WHERE buyer_id={buy_id} AND is_saved_for_later = FALSE;''')
        return purchase_id
