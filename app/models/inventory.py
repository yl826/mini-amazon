from flask import current_app as app
from .product import Product

class Inventory:
    def __init__(self, id, pid, name, cid, price, quantity, sid,category,  seller_firstname="", seller_lastname=""):
        self.id = id
        self.pid = pid
        self.name = name
        self.cid = cid
        self.price = price
        self.quantity = quantity
        self.seller_name = seller_firstname + " " + seller_lastname
        self.sid = sid
        self.category = category

    @staticmethod
    def get_products(pid, sid):
        rows = app.db.execute('''
        SELECT Inventory.id AS id, pid, Products.name AS name, cid, Inventory.price, quantity, sid, categories.name AS category
        FROM Inventory, Products
        join categories on products.cid = categories.id
        WHERE Inventory.pid = Products.id
        AND Inventory.pid=:pid
        AND Inventory.sid=:sid
        ''',
                              pid=pid,
                              sid=sid)
        return Inventory(*(rows[0]))

    @staticmethod
    def get_product_sid(sid):
        rows = app.db.execute('''
        SELECT Inventory.id AS id, pid, Products.name AS name, cid, Inventory.price, quantity, sid, categories.name AS category
        FROM Inventory, Products
        join categories on products.cid = categories.id
        WHERE Inventory.pid = Products.id
        AND Inventory.sid=:sid
        ''',
                              sid=sid)
        return [Inventory(*row) for row in rows]



    def get_product_pid(pid):
        rows = app.db.execute('''
        SELECT Inventory.id AS id, pid, Products.name AS name, cid, Inventory.price, quantity, sid, categories.name AS category
        FROM Inventory, Products
        join categories on products.cid = categories.id
        WHERE Inventory.pid = Products.id
        AND Inventory.pid=:pid
        ''',
                              pid=pid)
        return [Inventory(*row) for row in rows]

    @staticmethod
    def add_existed_product(pid, sid, price, quantity):
        try:
            rows = app.db.execute('''
            INSERT INTO Inventory(pid, sid, price, quantity)
            VALUES(:pid, :sid, :price, :quantity)
            ''',
                                  pid=pid,
                                  sid=sid,
                                  price=price,
                                  quantity=quantity)
        except Exception:
            rows = None
        if rows:
            return True
        else:
            return False

    @staticmethod
    def add_not_existed_product(name, cid, price, quantity, sid, description, image_path):
        try:
            pid = Product.add(name, sid, cid, description, image_path, price)
            rows = app.db.execute('''
                INSERT INTO Inventory(pid, sid, price, quantity)
                VALUES(:pid, :sid, :price, :quantity)
            ''',
                                  pid=pid,
                                  sid=sid,
                                  price=price,
                                  quantity=quantity)
        except Exception:
            rows = None
        if rows:
            return True
        else:
            return False

    @staticmethod
    def remove_product(sid, pid):
        rows = app.db.execute('''
            DELETE FROM Inventory
            WHERE pid = :pid
            AND sid = :sid
            ''',
                              pid=pid,
                              sid=sid)
        if rows:
            return True
        else:
            return False

    @staticmethod
    def change_quantity(pid, sid, quantity):
        rows = app.db.execute('''
            UPDATE Inventory
            SET quantity = :quantity
            WHERE pid = :pid
            AND sid = :sid
        ''',
                              pid=pid,
                              sid=sid,
                              quantity=quantity)
        if rows:
            return True
        else:
            return False

    @staticmethod
    def change_price(pid, sid, price):
        rows = app.db.execute('''
            UPDATE Inventory
            SET price = :price
            WHERE pid = :pid
            AND sid = :sid
        ''',
                              pid=pid,
                              sid=sid,
                              price=price)
        if rows:
            return True
        else:
            return False

    @staticmethod
    def get_all_sellers_for_product(pid: int):
        rows = app.db.execute(f'''
           SELECT Inventory.id, Inventory.pid AS pid, Products.name AS name, cid, Inventory.price, quantity, sid, Users.firstname AS seller_firstname, Users.lastname AS seller_lastname
           FROM Inventory, Products, Users
           WHERE Inventory.pid = Products.id AND Users.id = Inventory.sid
           AND Inventory.pid={pid}
           ORDER BY price ASC
           ''')
        return [Inventory(*row) for row in rows]

