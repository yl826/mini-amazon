from flask import current_app as app


class Purchase:
    def __init__(self, id, uid, time_purchased, fulfillment_status, total_amount, total_price):
        self.id = id
        self.uid = uid
        self.time_purchased = time_purchased

        self.fulfillment_status = fulfillment_status
        self.total_amount = total_amount
        self.total_price = total_price

    @staticmethod
    def get(id):
        rows = app.db.execute('''
                                SELECT p.id, buyer_id, time_purchased, bool_or(fulfillment_status) as fulfillment_status, count(o.qty) as total_amount, sum(o.qty * o.price_at_placement) as total_price
                                FROM purchases as p join orders as o on p.id = o.purchase_id
                                where p.id = :id
                                group by p.id
                            ''',
                              id=id)
        return Purchase(*(rows[0])) if rows else []

    @staticmethod
    def get_all_by_uid_since(uid, item_name, seller_name, start_date, end_date):
        rows = app.db.execute('''
                                SELECT p.id, buyer_id, time_purchased, bool_and(fulfillment_status) as fulfillment_status,
                                 sum(o.qty) as total_amount, sum(o.qty * o.price_at_placement) as total_price
                                FROM purchases as p 
                                join orders as o on p.id = o.purchase_id
                                join users as u on o.seller_id = u.id
                                join products on o.product_id = products.id
                                WHERE buyer_id = :uid
                                AND time_purchased between :start_date and :end_date
                                group by p.id
                                HAVING STRING_AGG (u.firstname || ' '||u.lastname, ',') like '%' || :seller_name || '%'
                                AND STRING_AGG (products.name, ',') like '%' || :item_name || '%'
                                ORDER BY time_purchased DESC

                                ''',
                              uid=uid,
                              item_name=item_name,
                              start_date=start_date,
                              seller_name=seller_name,
                              end_date=end_date)
        return [Purchase(*row) for row in rows] if rows else []


class Purchase_info:
    def __init__(self, id, uid, time_purchased, fulfillment_status, total_amount, total_price, address, firstname, lastname):
        self.id = id
        self.uid = uid
        self.time_purchased = time_purchased

        self.fulfillment_status = fulfillment_status
        self.total_amount = total_amount
        self.total_price = total_price
        self.address = address
        self.firstname = firstname
        self.lastname = lastname






    @staticmethod
    def get_all_by_status_id(sid, id, fulfillment):
        rows = app.db.execute('''
                            SELECT Purchases.id,
                                   Purchases.buyer_id                          AS uid,
                                   time_purchased,
                                   bool_or(fulfillment_status),
                                   sum(qty)                                         AS total_amount,
                                   sum(Orders.qty * Orders.price_at_placement) as total_price,
                                   Users.address as address,
                                   Users.firstname as firstname,
                                   Users.lastname as lastname
                            FROM Orders,
                                 Purchases
                            JOIN Users on Users.id = Purchases.buyer_id
                            WHERE Purchases.id = Orders.purchase_id
                              AND Orders.seller_id = :sid
                            group by Purchases.id, Users.address, Orders.fulfillment_status, Users.firstname, Users.lastname
                            HAVING (Purchases.id = :id or :id = -10000)
                              AND  (Orders.fulfillment_status = :fulfillment or :fulfillment is NULL )
                            ORDER BY Purchases.time_purchased DESC
            ''',
                              sid=sid,
                              id = id,
                              fulfillment = fulfillment)
        return [Purchase_info(*row) for row in rows]

