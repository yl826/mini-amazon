import datetime

from flask import current_app as app


class BalanceTransaction:
    def __init__(self, id, user_id, time_, amount, type_of_transaction):
        self.id = id
        self.user_id = user_id
        self.time_ = time_
        self.amount = amount
        self.type_of_transaction = type_of_transaction

    def serialize(self):
        print(self.time_)
        print(type(self.time_))
        print(str(self.time_))
        return {
            'id': self.id,
            'user_id': self.user_id,
            'time_': str(self.time_),
            'amount': float(self.amount),
            'type_of_transaction': self.type_of_transaction
        }


    @staticmethod
    def get_for_user(user_id: int, start_date: datetime=datetime.datetime.today(), end_date:datetime=datetime.datetime.today()+datetime.timedelta(days=1)):

        print(start_date)
        print(end_date)
        rows = app.db.execute('''
                    SELECT *
                    FROM balance_transaction
                    WHERE user_id = :user_id
                    AND
                    time_ BETWEEN :start_date AND :end_date
                ''', user_id=user_id, start_date = start_date, end_date = end_date)
        return [BalanceTransaction(*row) for row in rows]

    @staticmethod
    def get_balance_before_timestamp_by_user_id(user_id: int, time: datetime=datetime.datetime.today()):


        rows = app.db.execute('''
                    SELECT sum(amount)
                    FROM balance_transaction
                    WHERE user_id = :user_id
                    AND
                    time_ < :time
                ''', user_id=user_id, time = time)
        print(rows[0])
        return 0 if rows[0][0] is None else rows[0][0]

    @staticmethod
    def add_transaction(user_id: int, amount: int, type_of_transaction):
        rows = app.db.execute('''
                            INSERT INTO balance_transaction(user_id, amount, type_of_transaction)
                            VALUES(:user_id, :amount, :type_of_transaction)
                            RETURNING id
                ''', user_id=user_id, amount=amount, type_of_transaction=type_of_transaction)
        return rows
