from flask import current_app as app

class Product_Rank:
    def __init__(self, product_id, product_name, count):
        self.product_id = product_id
        self.product_name = product_name
        self.count = count

    @staticmethod
    def top_ten_products(sid):
        rows = app.db.execute('''
        SELECT Orders.product_id AS product_id, Products.name AS product_name, SUM(Orders.qty) AS count
        FROM Orders, Products 
        WHERE Orders.product_id = Products.id
        AND Orders.seller_id=:sid
        GROUP BY Orders.product_id, product_name
        ORDER BY count DESC
        LIMIT 10
        ''',
        sid=sid)
        return [Product_Rank(*row) for row in rows]

    @staticmethod
    def worst_ten_products(sid):
        rows = app.db.execute('''
        SELECT Orders.product_id AS product_id, Products.name AS product_name, SUM(Orders.qty) AS count
        FROM Orders, Products 
        WHERE Orders.product_id = Products.id
        AND Orders.seller_id=:sid
        GROUP BY Orders.product_id, product_name
        ORDER BY count ASC
        LIMIT 10
        ''',
        sid=sid)
        return [Product_Rank(*row) for row in rows]

    @staticmethod
    def lowest_ten_products(sid):
        rows = app.db.execute('''
        SELECT Inventory.pid AS product_id, Products.name AS product_name, SUM(Inventory.quantity) AS count
        FROM Inventory, Products 
        WHERE Inventory.pid = Products.id
        AND Inventory.sid=:sid
        GROUP BY product_id, product_name
        ORDER BY count ASC
        LIMIT 10
        ''',
        sid=sid)
        return [Product_Rank(*row) for row in rows]