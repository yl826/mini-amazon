from flask import current_app as app


class Categories:
    def __init__(self, cid, name):
        self.cid = cid
        self.name = name

    @staticmethod
    def get_all():
        rows = app.db.execute('''
            SELECT id AS cid, name AS name
            FROM Categories
        ''')
        return [Categories(*row) for row in rows]

    @staticmethod
    def get_by_id(cid: int):
        rows = app.db.execute('''
                SELECT id AS cid, name AS name
                FROM Categories
                WHERE id = :cid
            ''', cid=cid)
        return None if rows is None or len(rows) == 0 else Categories(*(rows[0]))
