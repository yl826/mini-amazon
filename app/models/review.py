from flask import current_app as app


class ProductReview:
    def __init__(self, id, buyer_id, product_id, rating, upvote, description, time_review):
        self.id = id
        self.buyer_id = buyer_id
        self.product_id = product_id
        self.rating = rating
        self.upvote = upvote
        self.description = description
        self.time_review = time_review

    @staticmethod
    def get_for_product(pid: int):
        rows = app.db.execute('''
             (SELECT *
                     FROM ReviewsForProducts
                     WHERE product_id = :pid
                     ORDER BY upvote DESC
                     LIMIT 3)
                    union all
                    (SELECT *
                     FROM ReviewsForProducts
                     WHERE product_id = :pid
                       AND id NOT IN (SELECT id
                                      FROM ReviewsForProducts
                                      WHERE product_id = :pid
                                      ORDER BY upvote DESC
                                      LIMIT 3)
                    ORDER BY time_review DESC)
        ''', pid=pid)
        return [ProductReview(*row) for row in rows]

    @staticmethod
    def get_for_user(user_id: int):
        rows = app.db.execute('''
            SELECT id, buyer_id, product_id, rating, upvote, description, time_review
            FROM ReviewsForProducts
            WHERE buyer_id = :user_id
        ''', user_id=user_id)
        return [ProductReview(*row) for row in rows]

    @staticmethod
    def get_for_product_and_buyer(product_id: int, buyer_id: int):
        rows = app.db.execute('''
                SELECT id, buyer_id, product_id, rating, upvote, description, time_review
                FROM ReviewsForProducts
                WHERE product_id = :product_id AND buyer_id = :buyer_id
            ''', product_id=product_id, buyer_id=buyer_id)
        return None if rows is None or len(rows) == 0 else ProductReview(*(rows[0]))

    @staticmethod
    def update_for_product_and_buyer(product_id: int, buyer_id: int, rating, description):
        rows = app.db.execute('''
                        UPDATE ReviewsForProducts SET (rating, description)=
                        (:rating, :description)
                        WHERE buyer_id = :buyer_id AND product_id=:product_id; ''',
                              buyer_id=buyer_id,
                              product_id=product_id,
                              rating=rating,
                              description=description)
        return None

    @staticmethod
    def update_upvote_for_id(review_id: int, offset: int, user_id: int):
        rows = app.db.execute('''
        
                        
                        INSERT INTO product_review_upvote_transaction
                        VALUES ((select nextval('product_review_upvote_transaction_id_seq')),:review_id,:user_id,current_timestamp, :offset)
                        ON CONFLICT (review_id,user_id) DO UPDATE 
                          SET time_ = current_timestamp , 
                              amount = :offset;
                        ''',
                              review_id=review_id,
                              offset=offset,
                              user_id=user_id,
                              )
        return None

    @staticmethod
    def delete_by_id(review_id: int):
        rows = app.db.execute('''
                            DELETE FROM ReviewsForProducts
                            WHERE id = :review_id; ''',
                              review_id=review_id)
        return None

    @staticmethod
    def add(buyer_id, product_id, rating, description):
        rows = app.db.execute('''
                INSERT INTO ReviewsForProducts(buyer_id, product_id, rating, description)
                VALUES(:buyer_id, :product_id, :rating, :description)
                ON CONFLICT (buyer_id, product_id) DO UPDATE 
                SET rating = :rating,
                    description = :description
                RETURNING id; ''', buyer_id=buyer_id,
                              product_id=product_id,
                              rating=rating,
                              description=description)
        id = rows[0][0]
        return id

    def isVoted(self, user_id: int):
        rows = app.db.execute('''
                        SELECT amount 
                        FROM product_review_upvote_transaction
                        WHERE review_id = :id
                        AND user_id = :user_id
                        ''', user_id=user_id,
                              id=self.id)
        print(0 if len(rows) == 0 else int(rows[0][0]))
        return 0 if len(rows) == 0 else int(rows[0][0])

    @staticmethod
    def get_total_number_by_id( product_id_list: list):
        rows = app.db.execute('''
                        SELECT product_id, count(*) 
                        FROM reviewsforproducts
                        WHERE product_id in :product_id_list
                        group by product_id
                        ''', product_id_list=tuple(product_id_list))

        return [] if len(rows) == 0 else rows


class SellerReview:
    def __init__(self, id, buyer_id, seller_id, rating, upvote, description, time_review):
        self.id = id
        self.buyer_id = buyer_id
        self.seller_id = seller_id
        self.rating = rating
        self.upvote = upvote
        self.description = description
        self.time_review = time_review

    # This one used in seller's public page, according to the requirement, it should show the top 3 most helpful then
    # show the most recent ones.
    @staticmethod
    def get_for_seller(seller_id: int):
        rows = app.db.execute('''
                   with reviews AS (
    SELECT *
    FROM reviewsforseller
    WHERE seller_id = :seller_id
    ORDER BY upvote DESC
),
     top3_reviews AS (
         SELECT *
         FROM reviews

         LIMIT 3
     )
    (SELECT *
     FROM top3_reviews
    )
union all
(SELECT *
 FROM reviews
 WHERE id NOT IN (Select id FROM top3_reviews)
 ORDER BY time_review DESC)
            ''', seller_id=seller_id)
        return [SellerReview(*row) for row in rows]

    @staticmethod
    def get_for_user(user_id: int):
        rows = app.db.execute('''
                SELECT *
                FROM reviewsforseller
                WHERE buyer_id = :user_id
            ''', user_id=user_id)
        return [SellerReview(*row) for row in rows]

    @staticmethod
    def get_for_product_and_buyer(seller_id: int, buyer_id: int):
        rows = app.db.execute('''
                SELECT id, buyer_id, seller_id, rating, upvote, description, time_review
                FROM reviewsforseller
                WHERE seller_id = :seller_id AND buyer_id = :buyer_id
            ''', seller_id=seller_id, buyer_id=buyer_id)
        return None if rows is None or len(rows) == 0 else SellerReview(*(rows[0]))

    @staticmethod
    def add(buyer_id, seller_id, rating, description):
        rows = app.db.execute('''
                INSERT INTO reviewsforseller(buyer_id, seller_id, rating, description)
                VALUES(:buyer_id, :seller_id, :rating, :description)
                ON CONFLICT (buyer_id, seller_id) DO UPDATE 
                SET rating = :rating,
                    description = :description
                RETURNING id; ''', buyer_id=buyer_id,
                              seller_id=seller_id,
                              rating=rating,
                              description=description)
        id = rows[0][0]
        return id

    def isVoted(self, user_id: int):
        rows = app.db.execute('''
                        SELECT amount 
                        FROM seller_review_upvote_transaction
                        WHERE review_id = :id
                        AND user_id = :user_id
                        ''', user_id=user_id,
                              id=self.id)
        return 0 if len(rows) == 0 else int(rows[0][0])

    @staticmethod
    def update_upvote_for_id(review_id: int, offset: int, user_id: int):
        rows = app.db.execute('''


                        INSERT INTO seller_review_upvote_transaction
                        VALUES ((select nextval('seller_review_upvote_transaction_id_seq')),:review_id,:user_id,current_timestamp, :offset)
                        ON CONFLICT (review_id,user_id) DO UPDATE 
                          SET time_ = current_timestamp , 
                              amount = :offset;
                        ''',
                              review_id=review_id,
                              offset=offset,
                              user_id=user_id,
                              )
        return None

    @staticmethod
    def delete_by_id(review_id: int):
        rows = app.db.execute('''
                            DELETE FROM reviewsforseller
                            WHERE id = :review_id; ''',
                              review_id=review_id)
        return None
