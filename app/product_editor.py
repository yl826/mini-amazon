from flask import render_template, request, abort
from flask_login import current_user

from .models.product import Product
from .models.category import Categories

from flask import Blueprint, redirect, url_for
import os

basedir = os.path.abspath(os.path.dirname(__file__))
bp = Blueprint('product_editor', __name__)


@bp.route('/product_editor', methods=['POST', 'GET'])
@bp.route('/product_editor/<pid>', methods=['GET'])
def product_editor(pid=None):
    if not current_user.is_authenticated:
        abort(404, description="Resource not found")


    jump_to_inventory = False
    if request.method == 'GET':
        jump_to_inventory = request.args.get('from', '') == 'inventory'
    if request.method == 'POST':
        if request.form['jump'] == "inventory":
            jump_to_inventory = True
        if 'image' in request.files:
            image_file = request.files['image']
            image_path = image_file.filename
            if image_path is None or image_path == '' or image_path == '/':
                image_path = None
            else:
                image_file.save(os.path.join(basedir, 'static', image_path))
        else:
            image_path = None
        if request.form['action'] == 'add':
            new_id = Product.add(request.form['pname'],
                                 current_user.id,
                                 int(request.form['category']),
                                 request.form['description'],
                                 image_path)
            if jump_to_inventory:
                return redirect(f"{url_for('inventory.addExistedProduct')}?pid={new_id}", code=302)
            else:
                return redirect(f"{url_for('product.product')}/{new_id}", code=302)
        elif request.form['action'] == 'update':
            pid = int(request.form['pid'])
            Product.update(pid,
                           request.form['pname'],
                           current_user.id,
                           int(request.form['category']),
                           request.form['description'],
                           image_path)
            if jump_to_inventory:
                return redirect(f"{url_for('inventory.addExistedProduct')}?pid={pid}", code=302)
            else:
                return redirect(f"{url_for('product.product')}/{pid}", code=302)
    dummy_product = Product(-1, "", 0, True, 0, "", "", -1, 0)
    page_title = 'Add New Product'
    action = 'add'
    if pid is not None:
        dummy_product = Product.get(pid)
        if current_user.id != dummy_product.creator_id:
            abort(404, description="Resource not found")
        page_title = f'Edit Product Information for {dummy_product.name}'
        action = 'update'
    # get all categories
    categories = Categories.get_all()
    # get all available sellers for the current product
    # inventory = Inventory.get_all_sellers_for_product(int(pid))
    # render the page by adding information to the index.html file
    return render_template('editProduct.html',
                           dummy_product=dummy_product,
                           categories=categories,
                           page_title=page_title,
                           action=action,
                           jump_to_inventory=jump_to_inventory)
