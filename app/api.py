import datetime

from flask import request, jsonify
from flask_login import current_user

from .models.balance_transaction import BalanceTransaction
from .models.order import Order
from flask import Blueprint, redirect, url_for

bp = Blueprint('api', __name__)


@bp.route('/api/balanceHistory', methods=['GET'])
def api_balance_history():
    if not current_user.is_authenticated:
        return redirect(url_for('index.index'))
    start_date = request.args.get('start_date')
    if start_date is None:
        start_date = datetime.datetime(1980, 9, 14, 0, 0, 0)
    end_date = request.args.get('start_date')
    if end_date is None:
        end_date = datetime.datetime.today() + datetime.timedelta(days=1)

    balance_transaction = BalanceTransaction.get_for_user(current_user.id, start_date, end_date)

    previous_balance = BalanceTransaction.get_balance_before_timestamp_by_user_id(current_user.id, start_date)
    return jsonify(transactions=[e.serialize() for e in balance_transaction], previous_balance=float(previous_balance))


@bp.route('/api/purchaseHistory/category', methods=['GET'])
def api_purchased_category():
    if not current_user.is_authenticated:
        return redirect(url_for('index.index'))
    purchased_category = Order.purchased_category(current_user.id)
    print(purchased_category[0])
    return jsonify([{
        "category_name": e[0],
        "category_purchased_times": e[1]
    } for e in purchased_category])
