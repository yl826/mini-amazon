from flask import abort, make_response
from flask_login import current_user
from flask import render_template, redirect, url_for
from wtforms import IntegerField, SelectField, SubmitField, validators
from wtforms.validators import NumberRange
from flask_wtf import FlaskForm


from .models.order import Order
from .models.purchase import Purchase, Purchase_info


from flask import Blueprint

bp = Blueprint('purchase_details', __name__)


@bp.route('/purchaseDetails/<purchase_id>', methods=['POST', 'GET'])
def purchase_details(purchase_id):
    if not current_user.is_authenticated:
        return redirect(url_for('index.index'))

    purchases = Purchase.get(purchase_id)

    if purchases and purchases.uid == current_user.id:
        orders = Order.get_all_related_oder_by_purchase_id(purchase_id)
        return render_template('purchase_details.html',
                               purchase_details=purchases, orders=orders)
    else:
        abort(404, description="Resource not found")



class SearchForm(FlaskForm):
    orderid = IntegerField('Purchase ID', validators=[validators.Optional(), NumberRange(min=0, message="Purchase ID Should >= 0")])
    fulfill = SelectField('Fulfillment status', choices=["All", "True", "False"])
    submit = SubmitField('Search')

@bp.route('/seller/purchaseDetails/<sid>', methods=['GET', 'POST'])
def purchase(sid):

    form = SearchForm()
    orderid = -10000 if form.orderid.data is None else form.orderid.data
    if form.fulfill.data == "True":
        fulfill = True
    elif form.fulfill.data == "False":
        fulfill = False
    else:
        fulfill = None
    print("full fill value is ", fulfill)
    purchase = Purchase_info.get_all_by_status_id(sid, orderid, fulfill)
    res = make_response(render_template("purchase_view.html", purchase = purchase,form=form))
    res.headers.add('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0')
    return res




